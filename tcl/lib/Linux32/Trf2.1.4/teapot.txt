Package Trf 2.1.4
Meta as::author      {Andreas Kupries}
Meta as::build::date 2014-01-10
Meta as::origin      http://sourceforge.net/projects/tcltrf
Meta category        Channel
Meta description     Provides a number of channel transformations
Meta license         BSD
Meta platform        linux-glibc2.3-ix86
Meta require         {Tcl 8.4}
Meta subject         channel encoding compression hashes transformation
Meta summary         Channel transformations
